from gcip import Job, Pipeline
from gcip.addons.container.images import PredefinedImages
from gcip.core.include import Include, IncludeArtifact
from gcip.core.job import TriggerJob, TriggerStrategy

generateJob = Job(
    stage="build",
    name="pipeline",
    script="/usr/src/app/docker/gcip.sh .gitlab-ci-child.py",
)
generateJob.set_image(PredefinedImages.GCIP)
generateJob.artifacts.add_paths("generated-config.yml")

triggerJob = TriggerJob(
    stage="deploy",
    name="pipeline",
    includes=IncludeArtifact(job="pipeline-build", artifact="generated-config.yml"),
    strategy=TriggerStrategy.DEPEND,
)
triggerJob.set_artifacts(generateJob.artifacts)
triggerJob.add_needs(generateJob)


Pipeline().add_children(generateJob, triggerJob).write_yaml()
